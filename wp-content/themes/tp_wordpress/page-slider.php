<?php
/**
 * Template Name: Slider
*/
get_header();
?>

<?php
	$slides = new WP_Query([
		'post_type'=> 'Slides',
		'orderby'=> 'date',
		'order'=> 'ASC'
		// 'category_name' => 'slug_de_la_categorie'
	]);

		
		
		

?>

<section id="carrousel">
	<div class="arrow left" id="left"><i class="fas fa-arrow-left"></i></div>
        <div id="carrousel_images">

			<?php
			$is_active = 'active';
			while( $slides->have_posts() ) {

				$slides->the_post();
				echo '<img src="' . get_the_post_thumbnail_url() . '" class="image ' . $is_active . '">';
				$is_active = '';
				echo "</img>";
			}
			?>
					

			<div id="puces">
				<div class="puce"></div>
				<div class="puce"></div>
				<div class="puce"></div>
			</div>
		</div>
	<div class="arrow right" id="right"><i class="fas fa-arrow-right"></i></div>
</section>


<?php
get_sidebar();
get_footer();
