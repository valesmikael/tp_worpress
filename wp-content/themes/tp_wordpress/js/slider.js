var 
    $left  = document.getElementById("left"),
    $right = document.getElementById("right"),
    $images = document.getElementsByClassName("image");

    $right.addEventListener("click", function(){

        var $active = document.getElementsByClassName("active")[0];
        var $next = $active.nextElementSibling;

        if( !$next ) $next = $images[0];

        $next.classList.add("active");
        $active.classList.remove("active");

    });

    $left.addEventListener("click", function(){

        var $active = document.getElementsByClassName("active")[0];
        var $prev = $active.previousElementSibling;

        if( !$prev ) $prev = $images[ $images.length -1 ];

        $prev.classList.add("active");
        $active.classList.remove("active");

    });
// Gestion des puces
var $puces = document.getElementsByClassName("puce");
    for( let i=0; i < $puces.length; i++ ) {

        let $puce = $puces[i];
        $puce.addEventListener("click", function(){

            var $active = document.getElementsByClassName("active")[0];
            var $new = $images[i];

            $active.classList.remove("active");
            $new.classList.add("active");

        });

    }
// Gestion automatiques
var mouse_is_over = false;
    let $carrousel = document.getElementById("carrousel");
    $carrousel.addEventListener("mouseenter", function(){
        mouse_is_over = true;
    })
    $carrousel.addEventListener("mouseleave", function(){
        mouse_is_over = false;
    })

    setInterval( function(){

        if( !mouse_is_over ) {
            // On trigger l'evenement click sur le bouton droite
            let event = new Event("click");
            $right.dispatchEvent( event );
        }
        
    }, 1500 );