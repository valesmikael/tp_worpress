<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package base-underscores
 */

get_header();
?>
<section>
	<!--
	have_posts() fonctionne comme mysqli_fetch_assoc().
	the_post() met en mémoire le post courrant
	-->
	<?php while( have_posts() ): the_post(); ?>
		<article>
			<!-- the_title() echo le titre -->
			<h2><?php the_title(); ?></h2>

			<!-- the_content() echo le contenu -->
			<div><?php the_content(); ?></div>
		</article>
	<?php endwhile; ?>
</section>

<?php
get_sidebar();
get_footer();
