<?php

    // Traitement de l'enregistrement
    if( !empty( $_POST ) ) {
        if( isset( $_POST[ 'home_featured_title' ] ) ) {

            $title = HOME_FEATURED_TITLE_DEFAULT;

            if( !empty( $_POST[ 'home_featured_title' ] ) ) {
                $title = $_POST[ 'home_featured_title' ];
            }

            update_option( 'home_featured_title', $title );
        }

        if( !empty( $_POST[ 'home_featured_category' ] ) ) {
            update_option( 'home_featured_category', $_POST[ 'home_featured_category' ] );
        }
    }

    $home_featured_title = get_option( 'home_featured_title', HOME_FEATURED_TITLE_DEFAULT );
    $home_featured_cat_id = get_option( 'home_featured_category', '-1' );
    $cats = get_categories();

    $featured_cats_options = '';
    foreach( $cats as $category ) {
        $featured_cats_options .= '<option value="' . $category->cat_ID . '"';
        $featured_cats_options .= lidem_get_selected_attr( (string) $category->cat_ID, $home_featured_cat_id );
        $featured_cats_options .= '>' . $category->cat_name . '</option>';
    }
?>
<div class="wrap">
    <h1>Options Beautiful l'IDEM</h1>
    <form action="" method="post" novalidate="novalidate">
        <table class="form-table">
            <!-- Titre de la mise en avant -->
            <tr>
                <th>Titre de mise en avant sur l'accueil</th>
                <td>
                    <input type="text" name="home_featured_title" value="<?php echo $home_featured_title; ?>">
                </td>
            </tr>
            <!-- Catégorie de mise en avant -->
            <tr>
                <th>Catégorie en avant sur l'accueil</th>
                <td>
                    <select name="home_featured_category">
                        <option value="-1"<?php lidem_echo_selected_attr( '-1', $home_featured_cat_id ); ?>>Choisissez un catégorie</option>
                        <?php echo $featured_cats_options; ?>
                    </select>
                </td>
            </tr>
        </table>

        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Enregistrer les modifications">
        </p>
    </form>
</div>