<?php

$post_id = get_the_ID();
$price = get_post_meta( $post_id, 'lidem_microwave_price', true );
$power = get_post_meta( $post_id, 'lidem_microwave_power', true );

?>
<label>
    <span>Prix: </span>
    <input type="text" name="lidem_microwave_price" value="<?php echo $price; ?>">
</label><br>
<label>
    <span>Puissance: </span>
    <select name="lidem_microwave_power">
        <option value="">Choisissez une puissance</option>
        <option value="700"<?php lidem_echo_selected_attr( '700', $power ); ?>>700 W</option>
        <option value="900"<?php lidem_echo_selected_attr( '900', $power ); ?>>900 W</option>
        <option value="1200"<?php lidem_echo_selected_attr( '1200', $power ); ?>>1200 W</option>
    </select>
</label>