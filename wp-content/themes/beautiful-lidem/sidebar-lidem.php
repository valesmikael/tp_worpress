<?php

if ( ! is_active_sidebar( 'lidem-sidebar' ) ) {
	return;
}
?>

<div id="lidem-sidebar" class="widget-area">
	<?php dynamic_sidebar( 'lidem-sidebar' ); ?>
</div>