<?php

// Constantes pour les valeurs par défaut
define( 'HOME_FEATURED_TITLE_DEFAULT', 'Articles en vedette');


function lidem_get_selected_attr( $option_value, $data_value ) {
    return $option_value !== $data_value ? '' : ' selected="selected"';
}

function lidem_echo_selected_attr( $option_value, $data_value ) {
    echo lidem_get_selected_attr( $option_value, $data_value );
}

function lidem_load_scripts_styles() {

    // Récupère l'adresse de la CSS du thème parent
    $parent_theme_url = get_template_directory_uri() . '/style.css';

    // Chargement de la CSS du thème parent
    wp_enqueue_style( 'parent-css', $parent_theme_url , [], true );

    // Chargement d'un JS
    wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/js/script.js', [], true );

}
add_action( 'wp_enqueue_scripts', 'lidem_load_scripts_styles' );

function lidem_register_menus() {

    register_nav_menus([
        'main-menu' => 'Menu principal',
        'legal-menu' => 'Bas de page'
    ]);

}
add_action( 'after_setup_theme', 'lidem_register_menus' );


/** Metabox **/
function lidem_display_microwave_spec_meta_box() {
    require_once 'admin/microwave-spec.php';
}

function lidem_microwave_spec_meta_boxes() {
    add_meta_box(
        'microwave_spec',
        'Caractéristiques Techniques',
        'lidem_display_microwave_spec_meta_box'
    );
}

function lidem_register_microwave_post_type() {
    register_post_type( 'microwave', [
        'labels' => [
            'name' => 'Micro-ondes',
            'singular_name' => 'Micro-onde',
            'add_new' => 'Nouveau micro-onde',
            'add_new_item' => 'Nouveau micro-onde',
            'edit_item' => 'Editer le micro-onde'
        ],
        'description' => 'Four micro-onde',
        'public' => true,
        'menu_position' => 21,
        'menu_icon' => 'dashicons-laptop',
        'supports' => [ 'title', 'editor', 'thumbnail' ],
        'register_meta_box_cb' => 'lidem_microwave_spec_meta_boxes'
    ]);
}
add_action( 'init', 'lidem_register_microwave_post_type' );


function lidem_manage_microwave_spec_meta_box( $post_id ) {
    if( !empty( $_POST[ 'lidem_microwave_price' ] ) ) {
        update_post_meta( $post_id, 'lidem_microwave_price', $_POST[ 'lidem_microwave_price' ] );
    }

    if( !empty( $_POST[ 'lidem_microwave_power' ] ) ) {
        update_post_meta( $post_id, 'lidem_microwave_power', $_POST[ 'lidem_microwave_power' ] );
    }
}
add_action( 'save_post', 'lidem_manage_microwave_spec_meta_box' );

// Sidebar supplémentaire
function lidem_widgets_init() {
	register_sidebar([
		'name'          => 'Sidebar de l\'IDEM',
		'id'            => 'lidem-sidebar',
		'description'   => 'Les widgets de l\'IDEM',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>'
    ]);
}
add_action( 'widgets_init', 'lidem_widgets_init' );


// Menu d'admin
function lidem_display_admin_menu_theme() {
    require_once 'admin/options.php';
}

function lidem_admin_menu_theme() {

	add_menu_page(
		'Options Beautiful l\'IDEM',
		'Beautiful l\'IDEM',
		'administrator',
		'beautiful-lidem-options',
		'lidem_display_admin_menu_theme',
		'dashicons-carrot',
		59
	);

}
add_action( 'admin_menu', 'lidem_admin_menu_theme' );
