<?php
/**
 * Template Name: Page d'accueil
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package base-underscores
 */

get_header();
?>

	<?php
		get_template_part( 'template-parts/content' );
		get_template_part( 'template-parts/content', 'alternatif' );

		get_sidebar( 'lidem' );
	?>

	<section>
		<?php if( have_posts() ): the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<div><?php the_content(); ?></div>
		<?php endif; ?>
	</section>

	<section>
		<h2>Les derniers micro-ondes</h2>
		<?php
			$microwaves = new WP_Query([
				'post_type'=> 'microwave',
				'orderby'=> 'date',
				'order'=> 'DESC'
				// 'category_name' => 'slug_de_la_categorie'
			]);

			while( $microwaves->have_posts() ) {
				$microwaves->the_post();

				$post_id = get_the_ID();
				$price = get_post_meta( $post_id, 'lidem_microwave_price', true );
				$power = get_post_meta( $post_id, 'lidem_microwave_power', true );
				
				echo '<h1>'; the_title(); echo '</h1>';
				echo '<div>'; the_content(); echo '</div>';
				echo '<div>Prix: ' . $price . ' €</div>';
				echo '<div>Puissance: ' . $power . ' W</div>';
			}
		?>
	</section>

	<section>
		<?php
			$home_featured_title = get_option( 'home_featured_title', HOME_FEATURED_TITLE_DEFAULT );
			$home_featured_cat_id = (int) get_option( 'home_featured_category', '-1' );
		?>
		<h2><?php echo $home_featured_title; ?></h2>
		<?php

			$posts_techno = new WP_Query([
				'post_type' => 'post',
				'orderby' => 'date',
				'order' => 'DESC',
				'cat' => $home_featured_cat_id
			]);

			while( $posts_techno->have_posts() ) {
				$posts_techno->the_post();
				echo '<article>';
				the_title( '<h3>', '</h3>' );
				the_content( '<div>', '</div>' );
				echo '</article>';
			}
		?>
	</section>

<?php

get_sidebar();
get_footer();
