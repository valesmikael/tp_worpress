<?php

/*
    Plugin Name: L'IDEM Shortcodes
    Author: L'IDEM
    Description: Des shortcodes totalement inutiles, mais osef !
    Version: 1.0.0
    License: GNU General Public License v2 or later
    License URI: LICENSE
    Text Domain: lidem-shortcodes
    Tags: fun-codes, bullshit-codes
*/

// Shortcodes
// Supersize
function lidem_supersize_shortcode( $atts = [], $content ) {

    $text = !empty( $content ) ? do_shortcode( $content ) : '';

    return '<span style="font-size: 72px; font-weight: 900; text-transform: uppercase;">' . $text . '</span>';

}

// Phone
function lidem_phone_shortcode( $atts = [], $content ) {

    $phone_atts = shortcode_atts([
        'number' => '',
        'font-size' => '16'
    ], $atts);

    $phone_content = !empty( $content ) ? do_shortcode( $content ) : 'Appelez !';

    $href = !empty( $phone_atts[ 'number' ] ) ? 'tel:' . $phone_atts[ 'number' ] : '#';
    $font_size = !empty( $phone_atts[ 'font-size' ] ) ? ' style="font-size:' . $phone_atts[ 'font-size' ] . 'px;"' : '';

    return '<a href="' . $href . '"' . $font_size . '>' . $phone_content . '</a>';
}

function lidem_shortcodes_init() {
    add_shortcode( 'supersize', 'lidem_supersize_shortcode' );
    add_shortcode( 'phone', 'lidem_phone_shortcode' );
}
add_action( 'init', 'lidem_shortcodes_init' );