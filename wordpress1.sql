-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.11 - MySQL Community Server (GPL)
-- SE du serveur:                Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour wordpress1
DROP DATABASE IF EXISTS `wordpress1`;
CREATE DATABASE IF NOT EXISTS `wordpress1` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wordpress1`;

-- Export de la structure de la table wordpress1. wp_1commentmeta
DROP TABLE IF EXISTS `wp_1commentmeta`;
CREATE TABLE IF NOT EXISTS `wp_1commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1commentmeta : ~0 rows (environ)
/*!40000 ALTER TABLE `wp_1commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_1commentmeta` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1comments
DROP TABLE IF EXISTS `wp_1comments`;
CREATE TABLE IF NOT EXISTS `wp_1comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1comments : ~0 rows (environ)
/*!40000 ALTER TABLE `wp_1comments` DISABLE KEYS */;
INSERT INTO `wp_1comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
	(1, 1, 'Un commentateur WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-10-04 09:20:41', '2018-10-04 07:20:41', 'Bonjour, ceci est un commentaire.\nPour débuter avec la modération, la modification et la suppression de commentaires, veuillez visiter l’écran des Commentaires dans le Tableau de bord.\nLes avatars des personnes qui commentent arrivent depuis <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0);
/*!40000 ALTER TABLE `wp_1comments` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1links
DROP TABLE IF EXISTS `wp_1links`;
CREATE TABLE IF NOT EXISTS `wp_1links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1links : ~0 rows (environ)
/*!40000 ALTER TABLE `wp_1links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_1links` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1options
DROP TABLE IF EXISTS `wp_1options`;
CREATE TABLE IF NOT EXISTS `wp_1options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1options : ~130 rows (environ)
/*!40000 ALTER TABLE `wp_1options` DISABLE KEYS */;
INSERT INTO `wp_1options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
	(1, 'siteurl', 'http://localhost/wordpress1', 'yes'),
	(2, 'home', 'http://localhost/wordpress1', 'yes'),
	(3, 'blogname', 'Cours Wordpress', 'yes'),
	(4, 'blogdescription', 'Un site utilisant WordPress', 'yes'),
	(5, 'users_can_register', '0', 'yes'),
	(6, 'admin_email', 'doudou@gmail.com', 'yes'),
	(7, 'start_of_week', '1', 'yes'),
	(8, 'use_balanceTags', '0', 'yes'),
	(9, 'use_smilies', '1', 'yes'),
	(10, 'require_name_email', '1', 'yes'),
	(11, 'comments_notify', '1', 'yes'),
	(12, 'posts_per_rss', '10', 'yes'),
	(13, 'rss_use_excerpt', '0', 'yes'),
	(14, 'mailserver_url', 'mail.example.com', 'yes'),
	(15, 'mailserver_login', 'login@example.com', 'yes'),
	(16, 'mailserver_pass', 'password', 'yes'),
	(17, 'mailserver_port', '110', 'yes'),
	(18, 'default_category', '1', 'yes'),
	(19, 'default_comment_status', 'open', 'yes'),
	(20, 'default_ping_status', 'open', 'yes'),
	(21, 'default_pingback_flag', '0', 'yes'),
	(22, 'posts_per_page', '10', 'yes'),
	(23, 'date_format', 'j F Y', 'yes'),
	(24, 'time_format', 'G \\h i \\m\\i\\n', 'yes'),
	(25, 'links_updated_date_format', 'j F Y G \\h i \\m\\i\\n', 'yes'),
	(26, 'comment_moderation', '0', 'yes'),
	(27, 'moderation_notify', '1', 'yes'),
	(28, 'permalink_structure', '', 'yes'),
	(29, 'rewrite_rules', '', 'yes'),
	(30, 'hack_file', '0', 'yes'),
	(31, 'blog_charset', 'UTF-8', 'yes'),
	(32, 'moderation_keys', '', 'no'),
	(33, 'active_plugins', 'a:1:{i:0;s:33:"classic-editor/classic-editor.php";}', 'yes'),
	(34, 'category_base', '', 'yes'),
	(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
	(36, 'comment_max_links', '2', 'yes'),
	(37, 'gmt_offset', '0', 'yes'),
	(38, 'default_email_category', '1', 'yes'),
	(39, 'recently_edited', 'a:3:{i:0;s:94:"D:\\__NE PAS SUPPRIMER - SAUVEGARDE\\UwAmp\\www\\wordpress1/wp-content/plugins/akismet/akismet.php";i:1;s:99:"D:\\__NE PAS SUPPRIMER - SAUVEGARDE\\UwAmp\\www\\wordpress1/wp-content/themes/twentyseventeen/style.css";i:2;s:0:"";}', 'no'),
	(40, 'template', 'base-underscores', 'yes'),
	(41, 'stylesheet', 'beautiful-lidem', 'yes'),
	(42, 'comment_whitelist', '1', 'yes'),
	(43, 'blacklist_keys', '', 'no'),
	(44, 'comment_registration', '0', 'yes'),
	(45, 'html_type', 'text/html', 'yes'),
	(46, 'use_trackback', '0', 'yes'),
	(47, 'default_role', 'subscriber', 'yes'),
	(48, 'db_version', '38590', 'yes'),
	(49, 'uploads_use_yearmonth_folders', '', 'yes'),
	(50, 'upload_path', '', 'yes'),
	(51, 'blog_public', '0', 'yes'),
	(52, 'default_link_category', '2', 'yes'),
	(53, 'show_on_front', 'page', 'yes'),
	(54, 'tag_base', '', 'yes'),
	(55, 'show_avatars', '1', 'yes'),
	(56, 'avatar_rating', 'G', 'yes'),
	(57, 'upload_url_path', '', 'yes'),
	(58, 'thumbnail_size_w', '150', 'yes'),
	(59, 'thumbnail_size_h', '150', 'yes'),
	(60, 'thumbnail_crop', '1', 'yes'),
	(61, 'medium_size_w', '300', 'yes'),
	(62, 'medium_size_h', '300', 'yes'),
	(63, 'avatar_default', 'mystery', 'yes'),
	(64, 'large_size_w', '1024', 'yes'),
	(65, 'large_size_h', '1024', 'yes'),
	(66, 'image_default_link_type', '', 'yes'),
	(67, 'image_default_size', '', 'yes'),
	(68, 'image_default_align', '', 'yes'),
	(69, 'close_comments_for_old_posts', '0', 'yes'),
	(70, 'close_comments_days_old', '14', 'yes'),
	(71, 'thread_comments', '1', 'yes'),
	(72, 'thread_comments_depth', '5', 'yes'),
	(73, 'page_comments', '0', 'yes'),
	(74, 'comments_per_page', '50', 'yes'),
	(75, 'default_comments_page', 'newest', 'yes'),
	(76, 'comment_order', 'asc', 'yes'),
	(77, 'sticky_posts', 'a:0:{}', 'yes'),
	(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
	(79, 'widget_text', 'a:5:{i:2;a:4:{s:5:"title";s:14:"Retrouvez-nous";s:4:"text";s:189:"<strong>Adresse</strong>\nAvenue des Champs-Élysées\n75008, Paris\n\n<strong>Heures d’ouverture</strong>\nDu lundi au vendredi : 9h00&ndash;17h00\nLes samedi et dimanche : 11h00&ndash;15h00";s:6:"filter";b:1;s:6:"visual";b:1;}i:3;a:4:{s:5:"title";s:20:"À propos de ce site";s:4:"text";s:99:"C’est peut-être le bon endroit pour vous présenter et votre site ou insérer quelques crédits.";s:6:"filter";b:1;s:6:"visual";b:1;}i:4;a:4:{s:5:"title";s:14:"Retrouvez-nous";s:4:"text";s:189:"<strong>Adresse</strong>\nAvenue des Champs-Élysées\n75008, Paris\n\n<strong>Heures d’ouverture</strong>\nDu lundi au vendredi : 9h00&ndash;17h00\nLes samedi et dimanche : 11h00&ndash;15h00";s:6:"filter";b:1;s:6:"visual";b:1;}i:5;a:4:{s:5:"title";s:20:"À propos de ce site";s:4:"text";s:99:"C’est peut-être le bon endroit pour vous présenter et votre site ou insérer quelques crédits.";s:6:"filter";b:1;s:6:"visual";b:1;}s:12:"_multiwidget";i:1;}', 'yes'),
	(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
	(81, 'uninstall_plugins', 'a:0:{}', 'no'),
	(82, 'timezone_string', 'Europe/Paris', 'yes'),
	(83, 'page_for_posts', '54', 'yes'),
	(84, 'page_on_front', '52', 'yes'),
	(85, 'default_post_format', '0', 'yes'),
	(86, 'link_manager_enabled', '0', 'yes'),
	(87, 'finished_splitting_shared_terms', '1', 'yes'),
	(88, 'site_icon', '0', 'yes'),
	(89, 'medium_large_size_w', '768', 'yes'),
	(90, 'medium_large_size_h', '0', 'yes'),
	(91, 'wp_page_for_privacy_policy', '3', 'yes'),
	(92, 'show_comments_cookies_opt_in', '0', 'yes'),
	(93, 'initial_db_version', '38590', 'yes'),
	(94, 'wp_1user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
	(95, 'fresh_site', '0', 'yes'),
	(96, 'WPLANG', 'fr_FR', 'yes'),
	(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
	(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
	(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
	(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
	(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
	(102, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:10:{i:0;s:6:"text-2";i:1;s:6:"text-3";i:2;s:6:"text-4";i:3;s:6:"text-5";i:4;s:8:"search-2";i:5;s:14:"recent-posts-2";i:6;s:17:"recent-comments-2";i:7;s:10:"archives-2";i:8;s:12:"categories-2";i:9;s:6:"meta-2";}s:13:"lidem-sidebar";a:0:{}s:9:"sidebar-1";a:2:{i:0;s:10:"calendar-2";i:1;s:15:"media_gallery-2";}s:13:"array_version";i:3;}', 'yes'),
	(103, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
	(104, 'widget_calendar', 'a:2:{i:2;a:1:{s:5:"title";s:23:"agenda des évènements";}s:12:"_multiwidget";i:1;}', 'yes'),
	(105, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
	(106, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
	(107, 'widget_media_gallery', 'a:2:{i:2;a:6:{s:5:"title";s:6:"photos";s:7:"columns";i:3;s:4:"size";s:9:"thumbnail";s:9:"link_type";s:4:"post";s:14:"orderby_random";b:0;s:3:"ids";a:1:{i:0;i:73;}}s:12:"_multiwidget";i:1;}', 'yes'),
	(108, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
	(109, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
	(110, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
	(111, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
	(112, 'cron', 'a:5:{i:1539012054;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1539026454;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1539069804;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1539077518;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
	(123, '_site_transient_timeout_browser_c91c259fd5f3fa8303f885fec872baad', '1539242605', 'no'),
	(124, '_site_transient_browser_c91c259fd5f3fa8303f885fec872baad', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:13:"69.0.3497.100";s:8:"platform";s:7:"Windows";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
	(138, 'can_compress_scripts', '1', 'no'),
	(140, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.9.8.zip";s:6:"locale";s:5:"fr_FR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.9.8.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.8";s:7:"version";s:5:"4.9.8";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1538983702;s:15:"version_checked";s:5:"4.9.8";s:12:"translations";a:0:{}}', 'no'),
	(141, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1538981542;s:7:"checked";a:3:{s:19:"akismet/akismet.php";s:5:"4.0.8";s:33:"classic-editor/classic-editor.php";s:3:"0.4";s:9:"hello.php";s:3:"1.7";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:3:{s:19:"akismet/akismet.php";O:8:"stdClass":9:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.0.8";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}}s:33:"classic-editor/classic-editor.php";O:8:"stdClass":9:{s:2:"id";s:28:"w.org/plugins/classic-editor";s:4:"slug";s:14:"classic-editor";s:6:"plugin";s:33:"classic-editor/classic-editor.php";s:11:"new_version";s:3:"0.4";s:3:"url";s:45:"https://wordpress.org/plugins/classic-editor/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/classic-editor.0.4.zip";s:5:"icons";a:2:{s:2:"2x";s:67:"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1750045";s:2:"1x";s:67:"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1750045";}s:7:"banners";a:2:{s:2:"2x";s:70:"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1750404";s:2:"1x";s:69:"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1751803";}s:11:"banners_rtl";a:0:{}}s:9:"hello.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/hello-dolly";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";s:5:"icons";a:2:{s:2:"2x";s:63:"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907";s:2:"1x";s:63:"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907";}s:7:"banners";a:1:{s:2:"1x";s:65:"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
	(143, 'classic-editor-replace', 'replace', 'yes'),
	(144, 'recently_activated', 'a:0:{}', 'yes'),
	(145, 'theme_mods_twentyseventeen', 'a:3:{s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:2:{s:3:"top";i:4;s:6:"social";i:3;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1539008711;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:10:{i:0;s:6:"text-2";i:1;s:6:"text-3";i:2;s:6:"text-4";i:3;s:6:"text-5";i:4;s:8:"search-2";i:5;s:14:"recent-posts-2";i:6;s:17:"recent-comments-2";i:7;s:10:"archives-2";i:8;s:12:"categories-2";i:9;s:6:"meta-2";}s:9:"sidebar-1";a:2:{i:0;s:10:"calendar-2";i:1;s:15:"media_gallery-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
	(156, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
	(176, 'theme_mods_wordpress1', 'a:3:{s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:0:{}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1538656166;s:4:"data";a:1:{s:19:"wp_inactive_widgets";a:10:{i:0;s:6:"text-2";i:1;s:6:"text-3";i:2;s:6:"text-4";i:3;s:6:"text-5";i:4;s:8:"search-2";i:5;s:14:"recent-posts-2";i:6;s:17:"recent-comments-2";i:7;s:10:"archives-2";i:8;s:12:"categories-2";i:9;s:6:"meta-2";}}}}', 'yes'),
	(178, 'current_theme', 'Beautiful L\'IDEM', 'yes'),
	(179, 'theme_switched', '', 'yes'),
	(186, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1539008705;s:7:"checked";a:7:{s:16:"base-underscores";s:5:"1.0.0";s:15:"beautiful-lidem";s:5:"1.0.0";s:13:"twentyfifteen";s:3:"2.0";s:15:"twentyseventeen";s:3:"1.7";s:13:"twentysixteen";s:3:"1.5";s:10:"wordpress1";s:5:"1.0.0";s:16:"wp_cours_starter";s:5:"1.0.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
	(188, 'theme_mods_wp_cours_starter', 'a:4:{i:0;b:0;s:18:"nav_menu_locations";a:3:{s:6:"menu-1";i:2;s:11:"menu-footer";i:3;s:11:"menu-header";i:4;}s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1538725547;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:10:{i:0;s:6:"text-2";i:1;s:6:"text-3";i:2;s:6:"text-4";i:3;s:6:"text-5";i:4;s:8:"search-2";i:5;s:14:"recent-posts-2";i:6;s:17:"recent-comments-2";i:7;s:10:"archives-2";i:8;s:12:"categories-2";i:9;s:6:"meta-2";}s:9:"sidebar-1";a:0:{}}}}', 'yes'),
	(204, 'theme_mods_beautifull-lidem', 'a:4:{i:0;b:0;s:18:"nav_menu_locations";a:3:{s:6:"menu-1";i:4;s:9:"main-menu";i:5;s:10:"legal-menu";i:5;}s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1539008317;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:10:{i:0;s:6:"text-2";i:1;s:6:"text-3";i:2;s:6:"text-4";i:3;s:6:"text-5";i:4;s:8:"search-2";i:5;s:14:"recent-posts-2";i:6;s:17:"recent-comments-2";i:7;s:10:"archives-2";i:8;s:12:"categories-2";i:9;s:6:"meta-2";}s:13:"lidem-sidebar";a:1:{i:0;s:15:"media_gallery-2";}s:9:"sidebar-1";a:1:{i:0;s:10:"calendar-2";}}}}', 'yes'),
	(230, 'category_children', 'a:0:{}', 'yes'),
	(240, '_site_transient_timeout_theme_roots', '1539010504', 'no'),
	(241, '_site_transient_theme_roots', 'a:7:{s:16:"base-underscores";s:7:"/themes";s:15:"beautiful-lidem";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";s:10:"wordpress1";s:7:"/themes";s:16:"wp_cours_starter";s:7:"/themes";}', 'no'),
	(243, '_transient_is_multi_author', '0', 'yes'),
	(246, 'theme_mods_beautiful-lidem', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:6:"menu-1";i:4;}}', 'yes');
/*!40000 ALTER TABLE `wp_1options` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1postmeta
DROP TABLE IF EXISTS `wp_1postmeta`;
CREATE TABLE IF NOT EXISTS `wp_1postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1postmeta : ~164 rows (environ)
/*!40000 ALTER TABLE `wp_1postmeta` DISABLE KEYS */;
INSERT INTO `wp_1postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
	(1, 2, '_wp_page_template', 'default'),
	(2, 3, '_wp_page_template', 'default'),
	(3, 5, '_wp_attached_file', '2018/10/espresso.jpg'),
	(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:20:"2018/10/espresso.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"espresso-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"espresso-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"espresso-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"espresso-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:20:"espresso-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(5, 5, '_starter_content_theme', 'twentyseventeen'),
	(6, 5, '_customize_draft_post_name', 'expresso'),
	(7, 6, '_wp_attached_file', '2018/10/sandwich.jpg'),
	(8, 6, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:20:"2018/10/sandwich.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"sandwich-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"sandwich-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"sandwich-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"sandwich-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:20:"sandwich-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(9, 6, '_starter_content_theme', 'twentyseventeen'),
	(10, 6, '_customize_draft_post_name', 'sandwich'),
	(11, 7, '_wp_attached_file', '2018/10/coffee.jpg'),
	(12, 7, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:18:"2018/10/coffee.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"coffee-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"coffee-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"coffee-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:19:"coffee-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:18:"coffee-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(13, 7, '_starter_content_theme', 'twentyseventeen'),
	(14, 7, '_customize_draft_post_name', 'cafe'),
	(15, 8, '_customize_draft_post_name', 'accueil'),
	(16, 8, '_customize_changeset_uuid', '9b0aa9ff-568f-4bd1-ad97-8318a310b245'),
	(17, 9, '_thumbnail_id', '6'),
	(18, 9, '_customize_draft_post_name', 'a-propos-de'),
	(19, 9, '_customize_changeset_uuid', '9b0aa9ff-568f-4bd1-ad97-8318a310b245'),
	(20, 10, '_thumbnail_id', '5'),
	(21, 10, '_customize_draft_post_name', 'contact'),
	(22, 10, '_customize_changeset_uuid', '9b0aa9ff-568f-4bd1-ad97-8318a310b245'),
	(23, 11, '_thumbnail_id', '7'),
	(24, 11, '_customize_draft_post_name', 'blog'),
	(25, 11, '_customize_changeset_uuid', '9b0aa9ff-568f-4bd1-ad97-8318a310b245'),
	(26, 12, '_thumbnail_id', '5'),
	(27, 12, '_customize_draft_post_name', 'une-section-de-page-daccueil'),
	(28, 12, '_customize_changeset_uuid', '9b0aa9ff-568f-4bd1-ad97-8318a310b245'),
	(29, 14, '_wp_attached_file', '2018/10/espresso-1.jpg'),
	(30, 14, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:22:"2018/10/espresso-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"espresso-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"espresso-1-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"espresso-1-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"espresso-1-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:22:"espresso-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(31, 14, '_starter_content_theme', 'twentyseventeen'),
	(32, 14, '_customize_draft_post_name', 'expresso'),
	(33, 15, '_wp_attached_file', '2018/10/sandwich-1.jpg'),
	(34, 15, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:22:"2018/10/sandwich-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"sandwich-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"sandwich-1-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"sandwich-1-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"sandwich-1-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:22:"sandwich-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(35, 15, '_starter_content_theme', 'twentyseventeen'),
	(36, 15, '_customize_draft_post_name', 'sandwich'),
	(37, 16, '_wp_attached_file', '2018/10/coffee-1.jpg'),
	(38, 16, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:20:"2018/10/coffee-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"coffee-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"coffee-1-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"coffee-1-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"coffee-1-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:20:"coffee-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(39, 16, '_starter_content_theme', 'twentyseventeen'),
	(40, 16, '_customize_draft_post_name', 'cafe'),
	(41, 17, '_customize_draft_post_name', 'accueil'),
	(42, 17, '_customize_changeset_uuid', '74d20519-5428-4ea5-b86d-caf157f92a0d'),
	(43, 18, '_thumbnail_id', '15'),
	(44, 18, '_customize_draft_post_name', 'a-propos-de'),
	(45, 18, '_customize_changeset_uuid', '74d20519-5428-4ea5-b86d-caf157f92a0d'),
	(46, 19, '_thumbnail_id', '14'),
	(47, 19, '_customize_draft_post_name', 'contact'),
	(48, 19, '_customize_changeset_uuid', '74d20519-5428-4ea5-b86d-caf157f92a0d'),
	(49, 20, '_thumbnail_id', '16'),
	(50, 20, '_customize_draft_post_name', 'blog'),
	(51, 20, '_customize_changeset_uuid', '74d20519-5428-4ea5-b86d-caf157f92a0d'),
	(52, 21, '_thumbnail_id', '14'),
	(53, 21, '_customize_draft_post_name', 'une-section-de-page-daccueil'),
	(54, 21, '_customize_changeset_uuid', '74d20519-5428-4ea5-b86d-caf157f92a0d'),
	(55, 13, '_customize_restore_dismissed', '1'),
	(101, 28, '_wp_attached_file', '2018/10/espresso-2.jpg'),
	(102, 28, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:22:"2018/10/espresso-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"espresso-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"espresso-2-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"espresso-2-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"espresso-2-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:22:"espresso-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(103, 28, '_starter_content_theme', 'twentyseventeen'),
	(104, 28, '_customize_draft_post_name', 'expresso'),
	(105, 29, '_wp_attached_file', '2018/10/sandwich-2.jpg'),
	(106, 29, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:22:"2018/10/sandwich-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"sandwich-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"sandwich-2-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"sandwich-2-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"sandwich-2-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:22:"sandwich-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(107, 29, '_starter_content_theme', 'twentyseventeen'),
	(108, 29, '_customize_draft_post_name', 'sandwich'),
	(109, 30, '_wp_attached_file', '2018/10/coffee-2.jpg'),
	(110, 30, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:20:"2018/10/coffee-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"coffee-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"coffee-2-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"coffee-2-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"coffee-2-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:20:"coffee-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(111, 30, '_starter_content_theme', 'twentyseventeen'),
	(112, 30, '_customize_draft_post_name', 'cafe'),
	(113, 31, '_customize_draft_post_name', 'accueil'),
	(114, 31, '_customize_changeset_uuid', 'd1a51e30-1b6b-40dc-8c5f-a083bc85312c'),
	(115, 32, '_thumbnail_id', '29'),
	(116, 32, '_customize_draft_post_name', 'a-propos-de'),
	(117, 32, '_customize_changeset_uuid', 'd1a51e30-1b6b-40dc-8c5f-a083bc85312c'),
	(118, 33, '_thumbnail_id', '28'),
	(119, 33, '_customize_draft_post_name', 'contact'),
	(120, 33, '_customize_changeset_uuid', 'd1a51e30-1b6b-40dc-8c5f-a083bc85312c'),
	(121, 34, '_thumbnail_id', '30'),
	(122, 34, '_customize_draft_post_name', 'blog'),
	(123, 34, '_customize_changeset_uuid', 'd1a51e30-1b6b-40dc-8c5f-a083bc85312c'),
	(124, 35, '_thumbnail_id', '28'),
	(125, 35, '_customize_draft_post_name', 'une-section-de-page-daccueil'),
	(126, 35, '_customize_changeset_uuid', 'd1a51e30-1b6b-40dc-8c5f-a083bc85312c'),
	(127, 22, '_customize_restore_dismissed', '1'),
	(128, 37, '_wp_attached_file', '2018/10/espresso-3.jpg'),
	(129, 37, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:22:"2018/10/espresso-3.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"espresso-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"espresso-3-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"espresso-3-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"espresso-3-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:22:"espresso-3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(130, 37, '_starter_content_theme', 'twentyseventeen'),
	(131, 37, '_customize_draft_post_name', 'expresso'),
	(132, 38, '_wp_attached_file', '2018/10/sandwich-3.jpg'),
	(133, 38, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:22:"2018/10/sandwich-3.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"sandwich-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"sandwich-3-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"sandwich-3-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"sandwich-3-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:22:"sandwich-3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(134, 38, '_starter_content_theme', 'twentyseventeen'),
	(135, 38, '_customize_draft_post_name', 'sandwich'),
	(136, 39, '_wp_attached_file', '2018/10/coffee-3.jpg'),
	(137, 39, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2000;s:6:"height";i:1200;s:4:"file";s:20:"2018/10/coffee-3.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"coffee-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"coffee-3-300x180.jpg";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"coffee-3-768x461.jpg";s:5:"width";i:768;s:6:"height";i:461;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"coffee-3-1024x614.jpg";s:5:"width";i:1024;s:6:"height";i:614;s:9:"mime-type";s:10:"image/jpeg";}s:32:"twentyseventeen-thumbnail-avatar";a:4:{s:4:"file";s:20:"coffee-3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(138, 39, '_starter_content_theme', 'twentyseventeen'),
	(139, 39, '_customize_draft_post_name', 'cafe'),
	(140, 40, '_customize_draft_post_name', 'accueil'),
	(141, 40, '_customize_changeset_uuid', 'ffdb7cb6-92f8-46b2-a486-52be74cb41b4'),
	(142, 41, '_thumbnail_id', '38'),
	(143, 41, '_customize_draft_post_name', 'a-propos-de'),
	(144, 41, '_customize_changeset_uuid', 'ffdb7cb6-92f8-46b2-a486-52be74cb41b4'),
	(145, 42, '_thumbnail_id', '37'),
	(146, 42, '_customize_draft_post_name', 'contact'),
	(147, 42, '_customize_changeset_uuid', 'ffdb7cb6-92f8-46b2-a486-52be74cb41b4'),
	(148, 43, '_thumbnail_id', '39'),
	(149, 43, '_customize_draft_post_name', 'blog'),
	(150, 43, '_customize_changeset_uuid', 'ffdb7cb6-92f8-46b2-a486-52be74cb41b4'),
	(151, 44, '_thumbnail_id', '37'),
	(152, 44, '_customize_draft_post_name', 'une-section-de-page-daccueil'),
	(153, 44, '_customize_changeset_uuid', 'ffdb7cb6-92f8-46b2-a486-52be74cb41b4'),
	(154, 36, '_customize_restore_dismissed', '1'),
	(157, 47, '_menu_item_type', 'post_type'),
	(158, 47, '_menu_item_menu_item_parent', '0'),
	(159, 47, '_menu_item_object_id', '1'),
	(160, 47, '_menu_item_object', 'post'),
	(161, 47, '_menu_item_target', ''),
	(162, 47, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
	(163, 47, '_menu_item_xfn', ''),
	(164, 47, '_menu_item_url', ''),
	(166, 48, '_menu_item_type', 'taxonomy'),
	(167, 48, '_menu_item_menu_item_parent', '0'),
	(168, 48, '_menu_item_object_id', '1'),
	(169, 48, '_menu_item_object', 'category'),
	(170, 48, '_menu_item_target', ''),
	(171, 48, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
	(172, 48, '_menu_item_xfn', ''),
	(173, 48, '_menu_item_url', ''),
	(175, 2, '_edit_lock', '1538664746:1'),
	(176, 45, '_customize_restore_dismissed', '1'),
	(177, 49, '_menu_item_type', 'custom'),
	(178, 49, '_menu_item_menu_item_parent', '0'),
	(179, 49, '_menu_item_object_id', '49'),
	(180, 49, '_menu_item_object', 'custom'),
	(181, 49, '_menu_item_target', ''),
	(182, 49, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
	(183, 49, '_menu_item_xfn', ''),
	(184, 49, '_menu_item_url', 'http://cscc'),
	(186, 50, '_menu_item_type', 'custom'),
	(187, 50, '_menu_item_menu_item_parent', '49'),
	(188, 50, '_menu_item_object_id', '50'),
	(189, 50, '_menu_item_object', 'custom'),
	(190, 50, '_menu_item_target', ''),
	(191, 50, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
	(192, 50, '_menu_item_xfn', ''),
	(193, 50, '_menu_item_url', 'http://ssss'),
	(195, 51, '_menu_item_type', 'custom'),
	(196, 51, '_menu_item_menu_item_parent', '49'),
	(197, 51, '_menu_item_object_id', '51'),
	(198, 51, '_menu_item_object', 'custom'),
	(199, 51, '_menu_item_target', ''),
	(200, 51, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
	(201, 51, '_menu_item_xfn', ''),
	(202, 51, '_menu_item_url', 'http://gggg'),
	(204, 52, '_edit_last', '1'),
	(205, 52, '_edit_lock', '1538740362:1'),
	(206, 54, '_edit_last', '1'),
	(207, 54, '_edit_lock', '1538740800:1'),
	(208, 52, '_wp_page_template', 'page-home.php'),
	(209, 54, '_wp_page_template', 'default'),
	(210, 57, '_edit_last', '1'),
	(211, 57, '_edit_lock', '1538984955:1'),
	(214, 61, '_edit_last', '1'),
	(215, 61, '_edit_lock', '1538984946:1'),
	(217, 63, '_menu_item_type', 'post_type'),
	(218, 63, '_menu_item_menu_item_parent', '0'),
	(219, 63, '_menu_item_object_id', '1'),
	(220, 63, '_menu_item_object', 'post'),
	(221, 63, '_menu_item_target', ''),
	(222, 63, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
	(223, 63, '_menu_item_xfn', ''),
	(224, 63, '_menu_item_url', ''),
	(226, 71, '_edit_last', '1'),
	(227, 71, '_edit_lock', '1538984665:1'),
	(228, 71, 'lidem_microwave_price', '89 '),
	(229, 71, 'lidem_microwave_power', '1200'),
	(232, 1, '_edit_last', '1'),
	(233, 1, '_encloseme', '1'),
	(234, 1, '_edit_lock', '1538984968:1'),
	(235, 73, '_wp_attached_file', 'guitare.jpg'),
	(236, 73, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1200;s:4:"file";s:11:"guitare.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"guitare-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"guitare-300x188.jpg";s:5:"width";i:300;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"guitare-768x480.jpg";s:5:"width";i:768;s:6:"height";i:480;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"guitare-1024x640.jpg";s:5:"width";i:1024;s:6:"height";i:640;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
	(237, 73, '_wp_attachment_image_alt', 'Good guitare!');
/*!40000 ALTER TABLE `wp_1postmeta` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1posts
DROP TABLE IF EXISTS `wp_1posts`;
CREATE TABLE IF NOT EXISTS `wp_1posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1posts : ~66 rows (environ)
/*!40000 ALTER TABLE `wp_1posts` DISABLE KEYS */;
INSERT INTO `wp_1posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
	(1, 1, '2018-10-04 09:20:41', '2018-10-04 07:20:41', 'Bienvenue sur WordPress. Ceci est votre premier article. Modifiez-le ou supprimez-le, puis lancez-vous !', 'Bonjour tout le monde !', '', 'publish', 'open', 'open', '', 'bonjour-tout-le-monde', '', '', '2018-10-08 09:49:28', '2018-10-08 07:49:28', '', 0, 'http://localhost/wordpress1/?p=1', 0, 'post', '', 1),
	(2, 1, '2018-10-04 09:20:41', '2018-10-04 07:20:41', 'Voici un exemple de page. Elle est différente d’un article de blog, en cela qu’elle restera à la même place, et s’affichera dans le menu de navigation de votre site (en fonction de votre thème). La plupart des gens commencent par écrire une page « À Propos » qui les présente aux visiteurs potentiels du site. Vous pourriez y écrire quelque chose de ce tenant :\n\n<blockquote>Bonjour ! Je suis un mécanicien qui aspire à devenir un acteur, et ceci est mon blog. J’habite à Bordeaux, j’ai un super chien qui s’appelle Russell, et j’aime la vodka-ananas (ainsi que perdre mon temps à regarder la pluie tomber).</blockquote>\n\n...ou bien quelque chose comme cela :\n\n<blockquote>La société 123 Machin Truc a été créée en 1971, et n’a cessé de proposer au public des machins-trucs de qualité depuis cette année. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules super pour la communauté bouzemontoise.</blockquote>\n\nÉtant donné que vous êtes un nouvel utilisateur ou une nouvelle utilisatrice de WordPress, vous devriez vous rendre sur votre <a href="http://localhost/wordpress1/wp-admin/">tableau de bord</a> pour effacer la présente page, et créer de nouvelles pages avec votre propre contenu. Amusez-vous bien !', 'Page d’exemple', '', 'publish', 'closed', 'open', '', 'page-d-exemple', '', '', '2018-10-04 09:20:41', '2018-10-04 07:20:41', '', 0, 'http://localhost/wordpress1/?page_id=2', 0, 'page', '', 0),
	(3, 1, '2018-10-04 09:20:41', '2018-10-04 07:20:41', '<h2>Qui sommes-nous ?</h2><p>L’adresse de notre site Web est : http://localhost/wordpress1.</p><h2>Utilisation des données personnelles collectées</h2><h3>Commentaires</h3><p>Quand vous laissez un commentaire sur notre site web, les données inscrites dans le formulaire de commentaire, mais aussi votre adresse IP et l’agent utilisateur de votre navigateur sont collectés pour nous aider à la détection des commentaires indésirables.</p><p>Une chaîne anonymisée créée à partir de votre adresse de messagerie (également appelée hash) peut être envoyée au service Gravatar pour vérifier si vous utilisez ce dernier. Les clauses de confidentialité du service Gravatar sont disponibles ici : https://automattic.com/privacy/. Après validation de votre commentaire, votre photo de profil sera visible publiquement à coté de votre commentaire.</p><h3>Médias</h3><p>Si vous êtes un utilisateur ou une utilisatrice enregistré·e et que vous téléversez des images sur le site web, nous vous conseillons d’éviter de téléverser des images contenant des données EXIF de coordonnées GPS. Les visiteurs de votre site web peuvent télécharger et extraire des données de localisation depuis ces images.</p><h3>Formulaires de contact</h3><h3>Cookies</h3><p>Si vous déposez un commentaire sur notre site, il vous sera proposé d’enregistrer votre nom, adresse de messagerie et site web dans des cookies. C’est uniquement pour votre confort afin de ne pas avoir à saisir ces informations si vous déposez un autre commentaire plus tard. Ces cookies expirent au bout d’un an.</p><p>Si vous avez un compte et que vous vous connectez sur ce site, un cookie temporaire sera créé afin de déterminer si votre navigateur accepte les cookies. Il ne contient pas de données personnelles et sera supprimé automatiquement à la fermeture de votre navigateur.</p><p>Lorsque vous vous connecterez, nous mettrons en place un certain nombre de cookies pour enregistrer vos informations de connexion et vos préférences d’écran. La durée de vie d’un cookie de connexion est de deux jours, celle d’un cookie d’option d’écran est d’un an. Si vous cochez « Se souvenir de moi », votre cookie de connexion sera conservé pendant deux semaines. Si vous vous déconnectez de votre compte, le cookie de connexion sera effacé.</p><p>En modifiant ou en publiant une publication, un cookie supplémentaire sera enregistré dans votre navigateur. Ce cookie ne comprend aucune donnée personnelle. Il indique simplement l’ID de la publication que vous venez de modifier. Il expire au bout d’un jour.</p><h3>Contenu embarqué depuis d’autres sites</h3><p>Les articles de ce site peuvent inclure des contenus intégrés (par exemple des vidéos, images, articles…). Le contenu intégré depuis d’autres sites se comporte de la même manière que si le visiteur se rendait sur cet autre site.</p><p>Ces sites web pourraient collecter des données sur vous, utiliser des cookies, embarquer des outils de suivis tiers, suivre vos interactions avec ces contenus embarqués si vous disposez d’un compte connecté sur leur site web.</p><h3>Statistiques et mesures d’audience</h3><h2>Utilisation et transmission de vos données personnelles</h2><h2>Durées de stockage de vos données</h2><p>Si vous laissez un commentaire, le commentaire et ses métadonnées sont conservés indéfiniment. Cela permet de reconnaître et approuver automatiquement les commentaires suivants au lieu de les laisser dans la file de modération.</p><p>Pour les utilisateurs et utilisatrices qui s’enregistrent sur notre site (si cela est possible), nous stockons également les données personnelles indiquées dans leur profil. Tous les utilisateurs et utilisatrices peuvent voir, modifier ou supprimer leurs informations personnelles à tout moment (à l’exception de leur nom d’utilisateur·ice). Les gestionnaires du site peuvent aussi voir et modifier ces informations.</p><h2>Les droits que vous avez sur vos données</h2><p>Si vous avez un compte ou si vous avez laissé des commentaires sur le site, vous pouvez demander à recevoir un fichier contenant toutes les données personnelles que nous possédons à votre sujet, incluant celles que vous nous avez fournies. Vous pouvez également demander la suppression des données personnelles vous concernant. Cela ne prend pas en compte les données stockées à des fins administratives, légales ou pour des raisons de sécurité.</p><h2>Transmission de vos données personnelles</h2><p>Les commentaires des visiteurs peuvent être vérifiés à l’aide d’un service automatisé de détection des commentaires indésirables.</p><h2>Informations de contact</h2><h2>Informations supplémentaires</h2><h3>Comment nous protégeons vos données</h3><h3>Procédures mises en œuvre en cas de fuite de données</h3><h3>Les services tiers qui nous transmettent des données</h3><h3>Opérations de marketing automatisé et/ou de profilage réalisées à l’aide des données personnelles</h3><h3>Affichage des informations liées aux secteurs soumis à des régulations spécifiques</h3>', 'Politique de confidentialité', '', 'draft', 'closed', 'open', '', 'politique-de-confidentialite', '', '', '2018-10-04 09:20:41', '2018-10-04 07:20:41', '', 0, 'http://localhost/wordpress1/?page_id=3', 0, 'page', '', 0),
	(4, 1, '2018-10-04 09:23:25', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-04 09:23:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?p=4', 0, 'post', '', 0),
	(5, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', '', 'Expresso', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:38:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/espresso.jpg', 0, 'attachment', 'image/jpeg', 0),
	(6, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', '', 'Sandwich', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:38:23', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/sandwich.jpg', 0, 'attachment', 'image/jpeg', 0),
	(7, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', '', 'Café', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:38:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/coffee.jpg', 0, 'attachment', 'image/jpeg', 0),
	(8, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', 'Bienvenue sur votre site ! C’est votre page d’accueil que vos visiteurs verront lorsqu’ils arriveront sur votre site la première fois.', 'Accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:38:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=8', 0, 'page', '', 0),
	(9, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', 'Vous pourriez être un artiste et vouloir présenter vos travaux et vous même ou encore être une entreprise avec une mission à promouvoir.', 'À propos de', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:38:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=9', 0, 'page', '', 0),
	(10, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', 'C’est une page avec des informations de contact de base, comme l’adresse et le numéro de téléphone. Vous pouvez aussi essayer une extension pour ajouter un formulaire de contact.', 'Contact', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:38:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=10', 0, 'page', '', 0),
	(11, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', '', 'Blog', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:38:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=11', 0, 'page', '', 0),
	(12, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', 'C’est un exemple de section de page d’accueil. Les sections de page d’accueil peuvent être n’importe quelle page autre que la page d’accueil elle-même, y compris la page qui affiche vos derniers articles de blog.', 'Une section de page d’accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:38:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=12', 0, 'page', '', 0),
	(13, 1, '2018-10-04 09:38:26', '0000-00-00 00:00:00', '{\n    "widget_text[2]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "widget_search[3]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "widget_text[3]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "sidebars_widgets[sidebar-1]": {\n        "starter_content": true,\n        "value": [\n            "text-2",\n            "search-3",\n            "text-3"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "widget_text[4]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "sidebars_widgets[sidebar-2]": {\n        "starter_content": true,\n        "value": [\n            "text-4"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "widget_text[5]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "widget_search[4]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "sidebars_widgets[sidebar-3]": {\n        "starter_content": true,\n        "value": [\n            "text-5",\n            "search-4"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menus_created_posts": {\n        "starter_content": true,\n        "value": [\n            5,\n            6,\n            7,\n            8,\n            9,\n            10,\n            11,\n            12\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu[-1]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu sup\\u00e9rieur"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-1]": {\n        "starter_content": true,\n        "value": {\n            "type": "custom",\n            "title": "Accueil",\n            "url": "http://localhost/wordpress1/",\n            "position": 0,\n            "nav_menu_term_id": -1,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-2]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 9,\n            "position": 1,\n            "nav_menu_term_id": -1,\n            "title": "\\u00c0 propos de"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-3]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 11,\n            "position": 2,\n            "nav_menu_term_id": -1,\n            "title": "Blog"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-4]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 10,\n            "position": 3,\n            "nav_menu_term_id": -1,\n            "title": "Contact"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "twentyseventeen::nav_menu_locations[top]": {\n        "starter_content": true,\n        "value": -1,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu[-5]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu des liens de r\\u00e9seaux sociaux"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-5]": {\n        "starter_content": true,\n        "value": {\n            "title": "Yelp",\n            "url": "https://www.yelp.com",\n            "position": 0,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-6]": {\n        "starter_content": true,\n        "value": {\n            "title": "Facebook",\n            "url": "https://www.facebook.com/wordpress",\n            "position": 1,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-7]": {\n        "starter_content": true,\n        "value": {\n            "title": "Twitter",\n            "url": "https://twitter.com/wordpress",\n            "position": 2,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-8]": {\n        "starter_content": true,\n        "value": {\n            "title": "Instagram",\n            "url": "https://www.instagram.com/explore/tags/wordcamp/",\n            "position": 3,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "nav_menu_item[-9]": {\n        "starter_content": true,\n        "value": {\n            "title": "E-mail",\n            "url": "mailto:wordpress@example.com",\n            "position": 4,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "twentyseventeen::nav_menu_locations[social]": {\n        "starter_content": true,\n        "value": -5,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "show_on_front": {\n        "starter_content": true,\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "page_on_front": {\n        "starter_content": true,\n        "value": 8,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "page_for_posts": {\n        "starter_content": true,\n        "value": 11,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "twentyseventeen::panel_1": {\n        "starter_content": true,\n        "value": 12,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "twentyseventeen::panel_2": {\n        "starter_content": true,\n        "value": 9,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "twentyseventeen::panel_3": {\n        "starter_content": true,\n        "value": 11,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    },\n    "twentyseventeen::panel_4": {\n        "starter_content": true,\n        "value": 10,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:38:26"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '9b0aa9ff-568f-4bd1-ad97-8318a310b245', '', '', '2018-10-04 09:38:26', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?p=13', 0, 'customize_changeset', '', 0),
	(14, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', '', 'Expresso', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:39:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/espresso-1.jpg', 0, 'attachment', 'image/jpeg', 0),
	(15, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', '', 'Sandwich', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:39:16', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/sandwich-1.jpg', 0, 'attachment', 'image/jpeg', 0),
	(16, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', '', 'Café', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:39:17', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/coffee-1.jpg', 0, 'attachment', 'image/jpeg', 0),
	(17, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', 'Bienvenue sur votre site ! C’est votre page d’accueil que vos visiteurs verront lorsqu’ils arriveront sur votre site la première fois.', 'Accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:39:18', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=17', 0, 'page', '', 0),
	(18, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', 'Vous pourriez être un artiste et vouloir présenter vos travaux et vous même ou encore être une entreprise avec une mission à promouvoir.', 'À propos de', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:39:18', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=18', 0, 'page', '', 0),
	(19, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', 'C’est une page avec des informations de contact de base, comme l’adresse et le numéro de téléphone. Vous pouvez aussi essayer une extension pour ajouter un formulaire de contact.', 'Contact', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:39:18', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=19', 0, 'page', '', 0),
	(20, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', '', 'Blog', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:39:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=20', 0, 'page', '', 0),
	(21, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', 'C’est un exemple de section de page d’accueil. Les sections de page d’accueil peuvent être n’importe quelle page autre que la page d’accueil elle-même, y compris la page qui affiche vos derniers articles de blog.', 'Une section de page d’accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:39:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=21', 0, 'page', '', 0),
	(22, 1, '2018-10-04 09:39:19', '0000-00-00 00:00:00', '{\n    "widget_text[6]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "widget_search[3]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "widget_text[7]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "sidebars_widgets[sidebar-1]": {\n        "starter_content": true,\n        "value": [\n            "text-6",\n            "search-3",\n            "text-7"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "widget_text[8]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "sidebars_widgets[sidebar-2]": {\n        "starter_content": true,\n        "value": [\n            "text-8"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "widget_text[9]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "widget_search[4]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "sidebars_widgets[sidebar-3]": {\n        "starter_content": true,\n        "value": [\n            "text-9",\n            "search-4"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menus_created_posts": {\n        "starter_content": true,\n        "value": [\n            14,\n            15,\n            16,\n            17,\n            18,\n            19,\n            20,\n            21\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu[-1]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu sup\\u00e9rieur"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-1]": {\n        "starter_content": true,\n        "value": {\n            "type": "custom",\n            "title": "Accueil",\n            "url": "http://localhost/wordpress1/",\n            "position": 0,\n            "nav_menu_term_id": -1,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-2]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 18,\n            "position": 1,\n            "nav_menu_term_id": -1,\n            "title": "\\u00c0 propos de"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-3]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 20,\n            "position": 2,\n            "nav_menu_term_id": -1,\n            "title": "Blog"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-4]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 19,\n            "position": 3,\n            "nav_menu_term_id": -1,\n            "title": "Contact"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "twentyseventeen::nav_menu_locations[top]": {\n        "starter_content": true,\n        "value": -1,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu[-5]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu des liens de r\\u00e9seaux sociaux"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-5]": {\n        "starter_content": true,\n        "value": {\n            "title": "Yelp",\n            "url": "https://www.yelp.com",\n            "position": 0,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-6]": {\n        "starter_content": true,\n        "value": {\n            "title": "Facebook",\n            "url": "https://www.facebook.com/wordpress",\n            "position": 1,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-7]": {\n        "starter_content": true,\n        "value": {\n            "title": "Twitter",\n            "url": "https://twitter.com/wordpress",\n            "position": 2,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-8]": {\n        "starter_content": true,\n        "value": {\n            "title": "Instagram",\n            "url": "https://www.instagram.com/explore/tags/wordcamp/",\n            "position": 3,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "nav_menu_item[-9]": {\n        "starter_content": true,\n        "value": {\n            "title": "E-mail",\n            "url": "mailto:wordpress@example.com",\n            "position": 4,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "twentyseventeen::nav_menu_locations[social]": {\n        "starter_content": true,\n        "value": -5,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "show_on_front": {\n        "starter_content": true,\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "page_on_front": {\n        "starter_content": true,\n        "value": 17,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "page_for_posts": {\n        "starter_content": true,\n        "value": 20,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "twentyseventeen::panel_1": {\n        "starter_content": true,\n        "value": 21,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "twentyseventeen::panel_2": {\n        "starter_content": true,\n        "value": 18,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "twentyseventeen::panel_3": {\n        "starter_content": true,\n        "value": 20,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    },\n    "twentyseventeen::panel_4": {\n        "starter_content": true,\n        "value": 19,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:39:19"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '74d20519-5428-4ea5-b86d-caf157f92a0d', '', '', '2018-10-04 09:39:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?p=22', 0, 'customize_changeset', '', 0),
	(28, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 'Expresso', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:51:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/espresso-2.jpg', 0, 'attachment', 'image/jpeg', 0),
	(29, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 'Sandwich', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:51:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/sandwich-2.jpg', 0, 'attachment', 'image/jpeg', 0),
	(30, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 'Café', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:51:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/coffee-2.jpg', 0, 'attachment', 'image/jpeg', 0),
	(31, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', 'Bienvenue sur votre site ! C’est votre page d’accueil que vos visiteurs verront lorsqu’ils arriveront sur votre site la première fois.', 'Accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:51:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=31', 0, 'page', '', 0),
	(32, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', 'Vous pourriez être un artiste et vouloir présenter vos travaux et vous même ou encore être une entreprise avec une mission à promouvoir.', 'À propos de', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:51:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=32', 0, 'page', '', 0),
	(33, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', 'C’est une page avec des informations de contact de base, comme l’adresse et le numéro de téléphone. Vous pouvez aussi essayer une extension pour ajouter un formulaire de contact.', 'Contact', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=33', 0, 'page', '', 0),
	(34, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 'Blog', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=34', 0, 'page', '', 0),
	(35, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', 'C’est un exemple de section de page d’accueil. Les sections de page d’accueil peuvent être n’importe quelle page autre que la page d’accueil elle-même, y compris la page qui affiche vos derniers articles de blog.', 'Une section de page d’accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=35', 0, 'page', '', 0),
	(36, 1, '2018-10-04 09:51:06', '0000-00-00 00:00:00', '{\n    "widget_text[6]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "widget_search[3]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "widget_text[7]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "sidebars_widgets[sidebar-1]": {\n        "starter_content": true,\n        "value": [\n            "text-6",\n            "search-3",\n            "text-7"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "widget_text[8]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "sidebars_widgets[sidebar-2]": {\n        "starter_content": true,\n        "value": [\n            "text-8"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "widget_text[9]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "widget_search[4]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "sidebars_widgets[sidebar-3]": {\n        "starter_content": true,\n        "value": [\n            "text-9",\n            "search-4"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menus_created_posts": {\n        "starter_content": true,\n        "value": [\n            28,\n            29,\n            30,\n            31,\n            32,\n            33,\n            34,\n            35\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu[-1]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu sup\\u00e9rieur"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-1]": {\n        "starter_content": true,\n        "value": {\n            "type": "custom",\n            "title": "Accueil",\n            "url": "http://localhost/wordpress1/",\n            "position": 0,\n            "nav_menu_term_id": -1,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-2]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 32,\n            "position": 1,\n            "nav_menu_term_id": -1,\n            "title": "\\u00c0 propos de"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-3]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 34,\n            "position": 2,\n            "nav_menu_term_id": -1,\n            "title": "Blog"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-4]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 33,\n            "position": 3,\n            "nav_menu_term_id": -1,\n            "title": "Contact"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "twentyseventeen::nav_menu_locations[top]": {\n        "starter_content": true,\n        "value": -1,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu[-5]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu des liens de r\\u00e9seaux sociaux"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-5]": {\n        "starter_content": true,\n        "value": {\n            "title": "Yelp",\n            "url": "https://www.yelp.com",\n            "position": 0,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-6]": {\n        "starter_content": true,\n        "value": {\n            "title": "Facebook",\n            "url": "https://www.facebook.com/wordpress",\n            "position": 1,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-7]": {\n        "starter_content": true,\n        "value": {\n            "title": "Twitter",\n            "url": "https://twitter.com/wordpress",\n            "position": 2,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-8]": {\n        "starter_content": true,\n        "value": {\n            "title": "Instagram",\n            "url": "https://www.instagram.com/explore/tags/wordcamp/",\n            "position": 3,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "nav_menu_item[-9]": {\n        "starter_content": true,\n        "value": {\n            "title": "E-mail",\n            "url": "mailto:wordpress@example.com",\n            "position": 4,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "twentyseventeen::nav_menu_locations[social]": {\n        "starter_content": true,\n        "value": -5,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "show_on_front": {\n        "starter_content": true,\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "page_on_front": {\n        "starter_content": true,\n        "value": 31,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "page_for_posts": {\n        "starter_content": true,\n        "value": 34,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "twentyseventeen::panel_1": {\n        "starter_content": true,\n        "value": 35,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "twentyseventeen::panel_2": {\n        "starter_content": true,\n        "value": 32,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "twentyseventeen::panel_3": {\n        "starter_content": true,\n        "value": 34,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    },\n    "twentyseventeen::panel_4": {\n        "starter_content": true,\n        "value": 33,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:51:06"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'd1a51e30-1b6b-40dc-8c5f-a083bc85312c', '', '', '2018-10-04 09:51:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?p=36', 0, 'customize_changeset', '', 0),
	(37, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 'Expresso', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:52:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/espresso-3.jpg', 0, 'attachment', 'image/jpeg', 0),
	(38, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 'Sandwich', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:52:07', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/sandwich-3.jpg', 0, 'attachment', 'image/jpeg', 0),
	(39, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 'Café', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-10-04 09:52:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/wp-content/uploads/2018/10/coffee-3.jpg', 0, 'attachment', 'image/jpeg', 0),
	(40, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', 'Bienvenue sur votre site ! C’est votre page d’accueil que vos visiteurs verront lorsqu’ils arriveront sur votre site la première fois.', 'Accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=40', 0, 'page', '', 0),
	(41, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', 'Vous pourriez être un artiste et vouloir présenter vos travaux et vous même ou encore être une entreprise avec une mission à promouvoir.', 'À propos de', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=41', 0, 'page', '', 0),
	(42, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', 'C’est une page avec des informations de contact de base, comme l’adresse et le numéro de téléphone. Vous pouvez aussi essayer une extension pour ajouter un formulaire de contact.', 'Contact', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=42', 0, 'page', '', 0),
	(43, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 'Blog', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=43', 0, 'page', '', 0),
	(44, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', 'C’est un exemple de section de page d’accueil. Les sections de page d’accueil peuvent être n’importe quelle page autre que la page d’accueil elle-même, y compris la page qui affiche vos derniers articles de blog.', 'Une section de page d’accueil', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?page_id=44', 0, 'page', '', 0),
	(45, 1, '2018-10-04 09:52:09', '0000-00-00 00:00:00', '{\n    "widget_text[6]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "widget_search[3]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "widget_text[7]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "sidebars_widgets[sidebar-1]": {\n        "starter_content": true,\n        "value": [\n            "text-6",\n            "search-3",\n            "text-7"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "widget_text[8]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjE0OiJSZXRyb3V2ZXotbm91cyI7czo0OiJ0ZXh0IjtzOjE4OToiPHN0cm9uZz5BZHJlc3NlPC9zdHJvbmc+CkF2ZW51ZSBkZXMgQ2hhbXBzLcOJbHlzw6llcwo3NTAwOCwgUGFyaXMKCjxzdHJvbmc+SGV1cmVzIGTigJlvdXZlcnR1cmU8L3N0cm9uZz4KRHUgbHVuZGkgYXUgdmVuZHJlZGnCoDogOWgwMCZuZGFzaDsxN2gwMApMZXMgc2FtZWRpIGV0IGRpbWFuY2hlwqA6IDExaDAwJm5kYXNoOzE1aDAwIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==",\n            "title": "Retrouvez-nous",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "76d2d1b7df11a545a2043d6d8d5ab653"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "sidebars_widgets[sidebar-2]": {\n        "starter_content": true,\n        "value": [\n            "text-8"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "widget_text[9]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjU6InRpdGxlIjtzOjIwOiLDgCBwcm9wb3MgZGUgY2Ugc2l0ZSI7czo0OiJ0ZXh0IjtzOjk5OiJD4oCZZXN0IHBldXQtw6p0cmUgbGUgYm9uIGVuZHJvaXQgcG91ciB2b3VzIHByw6lzZW50ZXIgZXQgdm90cmUgc2l0ZSBvdSBpbnPDqXJlciBxdWVscXVlcyBjcsOpZGl0cy4iO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9",\n            "title": "\\u00c0 propos de ce site",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "51c8510f5924736c919d45b3658ec84d"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "widget_search[4]": {\n        "starter_content": true,\n        "value": {\n            "encoded_serialized_instance": "YToxOntzOjU6InRpdGxlIjtzOjEwOiJSZWNoZXJjaGVyIjt9",\n            "title": "Rechercher",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "02ffedad0d6c8152bc7bb1d54294aa2e"\n        },\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "sidebars_widgets[sidebar-3]": {\n        "starter_content": true,\n        "value": [\n            "text-9",\n            "search-4"\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menus_created_posts": {\n        "starter_content": true,\n        "value": [\n            37,\n            38,\n            39,\n            40,\n            41,\n            42,\n            43,\n            44\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu[-1]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu sup\\u00e9rieur"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-1]": {\n        "starter_content": true,\n        "value": {\n            "type": "custom",\n            "title": "Accueil",\n            "url": "http://localhost/wordpress1/",\n            "position": 0,\n            "nav_menu_term_id": -1,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-2]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 41,\n            "position": 1,\n            "nav_menu_term_id": -1,\n            "title": "\\u00c0 propos de"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-3]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 43,\n            "position": 2,\n            "nav_menu_term_id": -1,\n            "title": "Blog"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-4]": {\n        "starter_content": true,\n        "value": {\n            "type": "post_type",\n            "object": "page",\n            "object_id": 42,\n            "position": 3,\n            "nav_menu_term_id": -1,\n            "title": "Contact"\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "twentyseventeen::nav_menu_locations[top]": {\n        "starter_content": true,\n        "value": -1,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu[-5]": {\n        "starter_content": true,\n        "value": {\n            "name": "Menu des liens de r\\u00e9seaux sociaux"\n        },\n        "type": "nav_menu",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-5]": {\n        "starter_content": true,\n        "value": {\n            "title": "Yelp",\n            "url": "https://www.yelp.com",\n            "position": 0,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-6]": {\n        "starter_content": true,\n        "value": {\n            "title": "Facebook",\n            "url": "https://www.facebook.com/wordpress",\n            "position": 1,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-7]": {\n        "starter_content": true,\n        "value": {\n            "title": "Twitter",\n            "url": "https://twitter.com/wordpress",\n            "position": 2,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-8]": {\n        "starter_content": true,\n        "value": {\n            "title": "Instagram",\n            "url": "https://www.instagram.com/explore/tags/wordcamp/",\n            "position": 3,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "nav_menu_item[-9]": {\n        "starter_content": true,\n        "value": {\n            "title": "E-mail",\n            "url": "mailto:wordpress@example.com",\n            "position": 4,\n            "nav_menu_term_id": -5,\n            "object_id": 0\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "twentyseventeen::nav_menu_locations[social]": {\n        "starter_content": true,\n        "value": -5,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "show_on_front": {\n        "starter_content": true,\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "page_on_front": {\n        "starter_content": true,\n        "value": 40,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "page_for_posts": {\n        "starter_content": true,\n        "value": 43,\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "twentyseventeen::panel_1": {\n        "starter_content": true,\n        "value": 44,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "twentyseventeen::panel_2": {\n        "starter_content": true,\n        "value": 41,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "twentyseventeen::panel_3": {\n        "starter_content": true,\n        "value": 43,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    },\n    "twentyseventeen::panel_4": {\n        "starter_content": true,\n        "value": 42,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2018-10-04 07:52:09"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'ffdb7cb6-92f8-46b2-a486-52be74cb41b4', '', '', '2018-10-04 09:52:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?p=45', 0, 'customize_changeset', '', 0),
	(47, 1, '2018-10-04 16:52:31', '2018-10-04 14:52:31', ' ', '', '', 'publish', 'closed', 'closed', '', '47', '', '', '2018-10-04 16:52:31', '2018-10-04 14:52:31', '', 0, 'http://localhost/wordpress1/?p=47', 1, 'nav_menu_item', '', 0),
	(48, 1, '2018-10-04 16:52:31', '2018-10-04 14:52:31', ' ', '', '', 'publish', 'closed', 'closed', '', '48', '', '', '2018-10-04 16:52:31', '2018-10-04 14:52:31', '', 0, 'http://localhost/wordpress1/?p=48', 2, 'nav_menu_item', '', 0),
	(49, 1, '2018-10-05 11:03:01', '2018-10-05 09:03:01', '', 'nos produits', '', 'publish', 'closed', 'closed', '', 'nos-produits', '', '', '2018-10-05 14:16:54', '2018-10-05 12:16:54', '', 0, 'http://localhost/wordpress1/?p=49', 2, 'nav_menu_item', '', 0),
	(50, 1, '2018-10-05 11:03:01', '2018-10-05 09:03:01', '', 'hifi', '', 'publish', 'closed', 'closed', '', 'hifi', '', '', '2018-10-05 14:16:54', '2018-10-05 12:16:54', '', 0, 'http://localhost/wordpress1/?p=50', 3, 'nav_menu_item', '', 0),
	(51, 1, '2018-10-05 11:03:01', '2018-10-05 09:03:01', '', 'cuisines', '', 'publish', 'closed', 'closed', '', 'cuisines', '', '', '2018-10-05 14:16:54', '2018-10-05 12:16:54', '', 0, 'http://localhost/wordpress1/?p=51', 4, 'nav_menu_item', '', 0),
	(52, 1, '2018-10-05 11:34:11', '2018-10-05 09:34:11', '<div>\r\n<div>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto unde ducimus harum molestias voluptatum fugit rem illum pariatur consequatur magnam, culpa incidunt, veniam, quaerat sunt rerum laborum recusandae numquam sint.</div>\r\n</div>', 'Bienvenue chez moi !', '', 'publish', 'closed', 'closed', '', 'bienvenue-chez-moi', '', '', '2018-10-05 13:39:48', '2018-10-05 11:39:48', '', 0, 'http://localhost/wordpress1/?page_id=52', 0, 'page', '', 0),
	(53, 1, '2018-10-05 11:34:11', '2018-10-05 09:34:11', '<div>\r\n<div>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto unde ducimus harum molestias voluptatum fugit rem illum pariatur consequatur magnam, culpa incidunt, veniam, quaerat sunt rerum laborum recusandae numquam sint.</div>\r\n</div>', 'Bienvenue chez moi !', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2018-10-05 11:34:11', '2018-10-05 09:34:11', '', 52, 'http://localhost/wordpress1/?p=53', 0, 'revision', '', 0),
	(54, 1, '2018-10-05 11:38:13', '2018-10-05 09:38:13', 'Voici mon produit', 'Nos produits', '', 'publish', 'closed', 'closed', '', 'nos-produits', '', '', '2018-10-05 13:55:40', '2018-10-05 11:55:40', '', 0, 'http://localhost/wordpress1/?page_id=54', 0, 'page', '', 0),
	(55, 1, '2018-10-05 11:38:13', '2018-10-05 09:38:13', 'glaglaglga', 'Nos produits', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-10-05 11:38:13', '2018-10-05 09:38:13', '', 54, 'http://localhost/wordpress1/?p=55', 0, 'revision', '', 0),
	(56, 1, '2018-10-05 13:55:40', '2018-10-05 11:55:40', 'Voici mon produit', 'Nos produits', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-10-05 13:55:40', '2018-10-05 11:55:40', '', 54, 'http://localhost/wordpress1/?p=56', 0, 'revision', '', 0),
	(57, 1, '2018-10-05 14:02:50', '2018-10-05 12:02:50', 'Bonne nuit', 'Bonsoir tous le monde', '', 'publish', 'open', 'open', '', 'bondoir-tous-le-monde', '', '', '2018-10-08 09:49:15', '2018-10-08 07:49:15', '', 0, 'http://localhost/wordpress1/?p=57', 0, 'post', '', 0),
	(58, 1, '2018-10-05 14:02:50', '2018-10-05 12:02:50', 'Bonne nuit', 'Bondoir tous le monde', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2018-10-05 14:02:50', '2018-10-05 12:02:50', '', 57, 'http://localhost/wordpress1/?p=58', 0, 'revision', '', 0),
	(59, 1, '2018-10-05 14:03:03', '2018-10-05 12:03:03', 'Bonne nuit', 'Bonsoir tous le monde', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2018-10-05 14:03:03', '2018-10-05 12:03:03', '', 57, 'http://localhost/wordpress1/?p=59', 0, 'revision', '', 0),
	(60, 1, '2018-10-05 14:03:20', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-05 14:03:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?p=60', 0, 'post', '', 0),
	(61, 1, '2018-10-05 14:04:02', '2018-10-05 12:04:02', 'il nage', 'toto a la plage', '', 'publish', 'open', 'open', '', 'toto-a-la-plage', '', '', '2018-10-08 09:49:05', '2018-10-08 07:49:05', '', 0, 'http://localhost/wordpress1/?p=61', 0, 'post', '', 0),
	(62, 1, '2018-10-05 14:04:02', '2018-10-05 12:04:02', 'il nage', 'toto a la plage', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2018-10-05 14:04:02', '2018-10-05 12:04:02', '', 61, 'http://localhost/wordpress1/?p=62', 0, 'revision', '', 0),
	(63, 1, '2018-10-05 14:16:51', '2018-10-05 12:16:51', '', 'Acceuil', '', 'publish', 'closed', 'closed', '', 'acceuil', '', '', '2018-10-05 14:16:54', '2018-10-05 12:16:54', '', 0, 'http://localhost/wordpress1/?p=63', 1, 'nav_menu_item', '', 0),
	(64, 1, '2018-10-05 14:42:19', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-05 14:42:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?post_type=microwave&p=64', 0, 'microwave', '', 0),
	(65, 1, '2018-10-05 14:59:21', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-05 14:59:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?post_type=microwave&p=65', 0, 'microwave', '', 0),
	(66, 1, '2018-10-05 14:59:35', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-05 14:59:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?post_type=microwave&p=66', 0, 'microwave', '', 0),
	(67, 1, '2018-10-05 15:00:36', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-05 15:00:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?post_type=microwave&p=67', 0, 'microwave', '', 0),
	(68, 1, '2018-10-05 15:04:35', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-05 15:04:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?post_type=microwave&p=68', 0, 'microwave', '', 0),
	(69, 1, '2018-10-05 15:05:04', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-05 15:05:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?post_type=microwave&p=69', 0, 'microwave', '', 0),
	(70, 1, '2018-10-05 15:06:00', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-05 15:06:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress1/?post_type=microwave&p=70', 0, 'microwave', '', 0),
	(71, 1, '2018-10-05 15:36:28', '2018-10-05 13:36:28', 'Micro-ondes design et performant !', 'Samsung MI6-CIA 20', '', 'publish', 'closed', 'closed', '', 'samsung-mi6-cia-20', '', '', '2018-10-08 09:43:53', '2018-10-08 07:43:53', '', 0, 'http://localhost/wordpress1/?post_type=microwave&#038;p=71', 0, 'microwave', '', 0),
	(72, 1, '2018-10-08 09:49:28', '2018-10-08 07:49:28', 'Bienvenue sur WordPress. Ceci est votre premier article. Modifiez-le ou supprimez-le, puis lancez-vous !', 'Bonjour tout le monde !', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-10-08 09:49:28', '2018-10-08 07:49:28', '', 1, 'http://localhost/wordpress1/?p=72', 0, 'revision', '', 0),
	(73, 1, '2018-10-08 10:48:50', '2018-10-08 08:48:50', 'modèle introuvable', 'guitare', 'lLspaul custom', 'inherit', 'open', 'closed', '', 'guitare', '', '', '2018-10-08 10:49:51', '2018-10-08 08:49:51', '', 0, 'http://localhost/wordpress1/wp-content/uploads/guitare.jpg', 0, 'attachment', 'image/jpeg', 0);
/*!40000 ALTER TABLE `wp_1posts` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1termmeta
DROP TABLE IF EXISTS `wp_1termmeta`;
CREATE TABLE IF NOT EXISTS `wp_1termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1termmeta : ~0 rows (environ)
/*!40000 ALTER TABLE `wp_1termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_1termmeta` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1terms
DROP TABLE IF EXISTS `wp_1terms`;
CREATE TABLE IF NOT EXISTS `wp_1terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1terms : ~5 rows (environ)
/*!40000 ALTER TABLE `wp_1terms` DISABLE KEYS */;
INSERT INTO `wp_1terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
	(1, 'Non classé', 'non-classe', 0),
	(4, 'header_menu', 'header_menu', 0),
	(5, 'menu-principale', 'menu-principale', 0),
	(6, 'Technologie', 'technologie', 0),
	(7, 'people', 'people', 0);
/*!40000 ALTER TABLE `wp_1terms` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1term_relationships
DROP TABLE IF EXISTS `wp_1term_relationships`;
CREATE TABLE IF NOT EXISTS `wp_1term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1term_relationships : ~9 rows (environ)
/*!40000 ALTER TABLE `wp_1term_relationships` DISABLE KEYS */;
INSERT INTO `wp_1term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
	(1, 6, 0),
	(47, 4, 0),
	(48, 4, 0),
	(49, 5, 0),
	(50, 5, 0),
	(51, 5, 0),
	(57, 6, 0),
	(61, 7, 0),
	(63, 5, 0);
/*!40000 ALTER TABLE `wp_1term_relationships` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1term_taxonomy
DROP TABLE IF EXISTS `wp_1term_taxonomy`;
CREATE TABLE IF NOT EXISTS `wp_1term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1term_taxonomy : ~5 rows (environ)
/*!40000 ALTER TABLE `wp_1term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_1term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
	(1, 1, 'category', '', 0, 0),
	(4, 4, 'nav_menu', '', 0, 2),
	(5, 5, 'nav_menu', '', 0, 4),
	(6, 6, 'category', '', 0, 2),
	(7, 7, 'category', '', 0, 1);
/*!40000 ALTER TABLE `wp_1term_taxonomy` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1usermeta
DROP TABLE IF EXISTS `wp_1usermeta`;
CREATE TABLE IF NOT EXISTS `wp_1usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1usermeta : ~21 rows (environ)
/*!40000 ALTER TABLE `wp_1usermeta` DISABLE KEYS */;
INSERT INTO `wp_1usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
	(1, 1, 'nickname', 'admin'),
	(2, 1, 'first_name', ''),
	(3, 1, 'last_name', ''),
	(4, 1, 'description', ''),
	(5, 1, 'rich_editing', 'true'),
	(6, 1, 'syntax_highlighting', 'true'),
	(7, 1, 'comment_shortcuts', 'false'),
	(8, 1, 'admin_color', 'fresh'),
	(9, 1, 'use_ssl', '0'),
	(10, 1, 'show_admin_bar_front', 'true'),
	(11, 1, 'locale', ''),
	(12, 1, 'wp_1capabilities', 'a:1:{s:13:"administrator";b:1;}'),
	(13, 1, 'wp_1user_level', '10'),
	(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice'),
	(15, 1, 'show_welcome_panel', '0'),
	(16, 1, 'session_tokens', 'a:1:{s:64:"0a1caa1b0e931c662c735dd22e9473edc60f55a9d0af7a9aaaa3559c6aa053e6";a:4:{s:10:"expiration";i:1539847404;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";s:5:"login";i:1538637804;}}'),
	(17, 1, 'wp_1dashboard_quick_press_last_post_id', '4'),
	(18, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
	(19, 1, 'metaboxhidden_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
	(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
	(21, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:"add-post_tag";i:1;s:15:"add-post_format";}'),
	(22, 1, 'nav_menu_recently_edited', '5'),
	(23, 1, 'wp_1user-settings', 'editor=tinymce&libraryContent=browse'),
	(24, 1, 'wp_1user-settings-time', '1538989725');
/*!40000 ALTER TABLE `wp_1usermeta` ENABLE KEYS */;

-- Export de la structure de la table wordpress1. wp_1users
DROP TABLE IF EXISTS `wp_1users`;
CREATE TABLE IF NOT EXISTS `wp_1users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Export de données de la table wordpress1.wp_1users : ~0 rows (environ)
/*!40000 ALTER TABLE `wp_1users` DISABLE KEYS */;
INSERT INTO `wp_1users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
	(1, 'admin', '$P$BiEhHRC7LTM8.6bW7TJH/NwekSLDy30', 'admin', 'doudou@gmail.com', '', '2018-10-04 07:20:41', '', 0, 'admin');
/*!40000 ALTER TABLE `wp_1users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
